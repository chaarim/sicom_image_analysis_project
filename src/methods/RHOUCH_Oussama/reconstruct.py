"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np

from src.forward_model import CFA
from src.methods.RHOUCH_Oussama.process_green_channel import process_green_channel
from src.methods.RHOUCH_Oussama.process_red_channel import process_red_channel
from src.methods.RHOUCH_Oussama.process_blue_channel import process_blue_channel

def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
    """Performs demosaicking on y.

    Args:
        y (np.ndarray): Mosaicked image to be reconstructed.
        cfa (str): Name of the CFA. Can be bayer or quad_bayer.

    Returns:
        np.ndarray: Demosaicked image.
    """
    # Performing the reconstruction.
    input_shape = (y.shape[0], y.shape[1], 3)
    op = CFA(cfa, input_shape)
    
    img_restored = op.adjoint(y)

    N, M = img_restored.shape[:2]
    
    img_restored = process_green_channel(img_restored, N, M)
    img_restored = process_red_channel(img_restored, N, M)
    img_restored = process_blue_channel(img_restored, N, M)
    
    return img_restored

####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
