import numpy as np  

def find_direct_neighbors(neighbors: list) -> list:
    """
    Calculate the gradients in horizontal, vertical, and diagonal directions for a pixel
    based on its neighboring pixels.

    Args:
        neighbors (list): The neighbors of a pixel in a 3x3 grid. The order of neighbors
                          is assumed to be [top-left, top, top-right, right, center, left,
                          bottom-left, bottom, bottom-right].

    Returns:
        list: The gradients [horizontal, vertical, diagonal-x, diagonal-y].
    """
    
    # Horizontal gradient: Difference between the right and left neighbors
    horizontal_gradient = (neighbors[3] - neighbors[5]) / 2

    # Vertical gradient: Difference between the top and bottom neighbors
    vertical_gradient = (neighbors[1] - neighbors[7]) / 2

    # Diagonal gradient along x-axis: Difference between top-right and bottom-left neighbors
    diagonal_gradient_x = (neighbors[2] - neighbors[6]) / (2 * np.sqrt(2))

    # Diagonal gradient along y-axis: Difference between top-left and bottom-right neighbors
    diagonal_gradient_y = (neighbors[0] - neighbors[8]) / (2 * np.sqrt(2))
    
    return [horizontal_gradient, vertical_gradient, diagonal_gradient_x, diagonal_gradient_y]
