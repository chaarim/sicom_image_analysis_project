def interpolate(neighbors: list, weights: list) -> float:
    """
    Interpolate the pixel value.
    
    Args:
        neighbors (list): The neighbors of the pixel.
        weights (list): The weights of the direct neighbors.
        
    Returns:
        float: The interpolated value.
    """
    
    return (weights[1] * neighbors[1] + weights[3] * neighbors[3] + weights[4] * neighbors[5] + weights[6] * neighbors[7]) / (weights[1] + weights[1] + weights[4] + weights[6])
  