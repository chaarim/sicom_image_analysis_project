import numpy as np
from .find_neighbors import find_neighbors
from .find_direct_neighbors import find_direct_neighbors
from .find_weights import find_weights
from .interpolate_neighboring import interpolate_neighboring

def process_interpolation(img, channel_index, N, M):
    """
    Process specific interpolation for a given channel.

    Args:
        img (np.ndarray): The image to process.
        channel_index (int): The index of the channel to process (0 for red, 2 for blue).
        N (int): Height of the image.
        M (int): Width of the image.

    Returns:
        np.ndarray: The image with the specified channel processed.
    """

    start_i = 1 if channel_index == 0 else 0  # Start from 1 for red and 0 for blue
    start_j = 0 if channel_index == 0 else 1  # Start from 0 for red and 1 for blue

    for i in range(start_i, N, 2):
        for j in range(start_j, M, 2):
            neighbors_0 = find_neighbors(img, channel_index, i, j, N, M)
            neighbors_1 = find_neighbors(img, 1, i, j, N, M)
            direct_neighbors = find_direct_neighbors(neighbors_1)
            weights = find_weights(img, direct_neighbors, 1, i, j, N, M)
            img[i, j, channel_index] = interpolate_neighboring(neighbors_0, neighbors_1, weights)

    return img
