import numpy as np

def find_neighbors(img: np.ndarray, channel: int, i: int, j: int, N: int, M: int) -> list:
    """
    Retrieve the 3x3 neighborhood of a pixel in a specified channel of an image.
    The function wraps around the image edges to handle border pixels.

    Args:
        img (np.ndarray): The image to process.
        channel (int): The index of the channel to process.
        i (int): The row index of the pixel.
        j (int): The column index of the pixel.
        N (int): Height of the image.
        M (int): Width of the image.
        
    Returns:
        np.ndarray: An array of neighbor pixel values in the specified channel.
    """
    
    neighbors = []

    for di in [-1, 0, 1]:
        for dj in [-1, 0, 1]:
            neighbor_i = (i + di) % N
            neighbor_j = (j + dj) % M

            neighbors.append(img[neighbor_i, neighbor_j, channel])

    return np.array(neighbors)
