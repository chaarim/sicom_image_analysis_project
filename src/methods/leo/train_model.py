import tensorflow as tf
from tensorflow.keras import layers, models
import numpy as np
import matplotlib.pyplot as plt

with open('array.npy', 'rb') as f:
    tableau_image_or = np.load(f)
    
with open('array_z.npy', 'rb') as f:
    tableau_images_z = np.load(f)

X = tableau_images_z.transpose((3,0,1,2))
y = tableau_image_or.transpose((3,0,1,2))

# Création du modèle
model = models.Sequential()

# Encoder
model.add(layers.Conv2D(32, (3, 3), activation='relu', padding='same', input_shape=(1024, 1024, 3)))
model.add(layers.MaxPooling2D((2, 2), padding='same'))

# Decoder
model.add(layers.Conv2D(32, (3, 3), activation='relu', padding='same'))
model.add(layers.UpSampling2D((2, 2)))
model.add(layers.Conv2D(3, (3, 3), activation='sigmoid', padding='same'))

# Compiler le modèle
model.compile(optimizer='adam', loss='mse')  # Mean Squared Error comme fonction de perte

# Entraînement du modèle
model.fit(X, y, epochs=3, batch_size=10, validation_split=0.2)

# Évaluation du modèle
loss = model.evaluate(X, y)
print(f"Loss sur l'ensemble de données : {loss}")

# Faire des prédictions
model.save("model")


