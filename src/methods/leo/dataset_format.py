import cv2
import os
import numpy as np
from src.forward_model import CFA
from src.utils import load_image, save_image, psnr, ssim
image_path = 'images/img_1.png'

img = load_image(image_path)
op = CFA('bayer', img.shape)
dossier_images = "dataset"


images_redimensionnees = []


for fichier in os.listdir(dossier_images):

    if fichier.endswith(".jpg"):
    
        chemin_image = os.path.join(dossier_images, fichier)
        image = cv2.imread(chemin_image)
        
        
        image_redimensionnee = cv2.resize(image, (1024, 1024))
        
       
        images_redimensionnees.append(image_redimensionnee)


tableau_images_or = np.stack(images_redimensionnees, axis=-1)


print("Dimensions du tableau d'images :", tableau_images_or.shape)
with open('array.npy', 'wb') as f:
    np.save(f, tableau_images_or)

images_redimensionnees = []


for k in range(500):
    
   
    image = tableau_images_or[:,:,:,k]
        
    
    y = op.direct(image)
    z = op.adjoint(y)
    
    images_redimensionnees.append(z)
tableau_images = np.empty((1024, 1024, 3, len(images_redimensionnees)), dtype=np.uint8)

# Remplissez le tableau avec les images de la liste
for i, image in enumerate(images_redimensionnees):
    print(i)
    tableau_images[:, :, :, i] = image
with open('array_z.npy', 'wb') as f:
    np.save(f, tableau_images)