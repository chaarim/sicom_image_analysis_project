# Image Analysis Project

Numerous RGB cameras in the commercial sector employ Color Filter Array (CFA) technology.
This technology involves an array of red, green, or blue filters placed atop the sensors, usually
organized in periodic patterns. The incident light is consequently filtered by each filter, before
being captured by the sensor. The typical process of acquiring images utilizes a predefined CFA
pattern to allocate a color to each pixel of the sensor. The goal of this code project is to demosaicing these CFA Image.

You can check the report called **Image_Analysis_Project_Report_Brice_Convers** to find more information about it.

## How to use

To use the code project you can move the **main_template.py** outside the **src** file and execute it.
Or you can also call the **run_reconstruction** method in you programme.

```Python

import src.methods.brice_convers.dataHandler as DataHandler
import src.methods.brice_convers.dataEvaluation as DataEvaluation
import time

WORKING_DIRECOTRY_PATH = "SICOM_Image_Analysis/sicom_image_analysis_project/"

DataHandler = DataHandler.DataHandler(WORKING_DIRECOTRY_PATH)
DataEvaluation = DataEvaluation.DataEvaluation(DataHandler)

def main(DataHandler):

    IMAGE_PATH = WORKING_DIRECOTRY_PATH + "images/"

    CFA_NAME = "quad_bayer"

    METHOD = "menon"

    startTime = time.time()

    DataHandler.list_images(IMAGE_PATH)

    DataHandler.print_list_images()

    DataHandler.compute_CFA_images(CFA_NAME)

    DataHandler.compute_reconstruction_images(METHOD, {"cfa": CFA_NAME})

    #The first agurment (ex: 3) is the image index in the list print by "DataHandler.print_list_images()"
    DataHandler.plot_reconstructed_image(3, METHOD, {"cfa": CFA_NAME}, zoomSize="large")

    DataEvaluation.print_metrics(3, METHOD)

    endTime = time.time()

    print("[INFO] Elapsed time: " + str(endTime - startTime) + "s")

    print("[INFO] End")

if __name__ == "__main__":
    main(DataHandler)

```

## TODO List:

- Fix menon method pour quad bayer pattern with landscape picture







## References:

[1] [*Research Paper:*](https://ieeexplore.ieee.org/document/4032820) Used for Menon Method.

## Authors
- [Brice Convers](https://briceconvers.com)
