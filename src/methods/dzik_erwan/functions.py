import numpy as np 
from scipy.ndimage import convolve 

from src.forward_model import CFA

def malvar2004(y,op): 
    "Code inspiered by KelSolaar's GitHub repo:"
    "https://github.com/colour-science/colour-demosaicing/tree/master"

    z = op.adjoint(y)
    mask = op.mask

    R = z[:,:,0]
    G = z[:,:,1]
    B = z[:,:,2]

    R_m, G_m, B_m = mask[:,:,0], mask[:,:,1], mask[:,:,2]

    
    G = np.where(np.logical_or(R_m == 1, B_m == 1), convolve(y, GR_GB), G)


    #Logical array that verify the conditon of the different filters 
    # Red rows.
    R_r = np.transpose(np.any(R_m == 1, axis=1)[None]) * np.ones(R.shape)
    # Red columns.
    R_c = np.any(R_m == 1, axis=0)[None] * np.ones(R.shape)
    # Blue rows.
    B_r = np.transpose(np.any(B_m == 1, axis=1)[None]) * np.ones(B.shape)
    # Blue columns
    B_c = np.any(B_m == 1, axis=0)[None] * np.ones(B.shape)

    #Results of the convolutions
    RBg_RBBR = convolve(y, Rg_RB_Bg_BR)
    RBg_BRRB = convolve(y, Rg_BR_Bg_RB)
    RBgr_BBRR = convolve(y, Rb_BB_Br_RR)

    
    R = np.where(np.logical_and(R_r == 1, B_c == 1), RBg_RBBR, R)
    R = np.where(np.logical_and(B_r == 1, R_c == 1), RBg_BRRB, R)

    B = np.where(np.logical_and(B_r == 1, R_c == 1), RBg_RBBR, B)
    B = np.where(np.logical_and(R_r == 1, B_c == 1), RBg_BRRB, B)

    R = np.where(np.logical_and(B_r == 1, B_c == 1), RBgr_BBRR, R)
    B = np.where(np.logical_and(R_r == 1, R_c == 1), RBgr_BBRR, B)

    
    res = np.clip(np.stack((R,G,B), axis = 2), 0, 1)


    return res

def quad_bayer_to_bayer(cfa_quad_bayer):
    "Quad to Bayer by swapping method"

    cfa_bayer = np.flip(np.copy(cfa_quad_bayer), 1)
    h,w = cfa_bayer.shape
    for col in range(1, w, 4):
        temp = np.copy(cfa_bayer[:, col])
        cfa_bayer[:, col] = cfa_bayer[:, col + 1]
        cfa_bayer[:, col + 1] = temp
    del temp


    for row in range(1, h, 4):
        temp = np.copy(cfa_bayer[row, :])
        cfa_bayer[row, :] = cfa_bayer[row + 1, :]
        cfa_bayer[row + 1, :] = temp
    del temp

    for row in range(1, h, 4):
        for col in range(2, w, 4):
            g1 = cfa_bayer[row, col]
            cfa_bayer[row, col] = cfa_bayer[row + 1, col -1]
            cfa_bayer[row + 1, col -1] = g1

    return np.flip(cfa_bayer,1)

# G at R locations or G at B locations
GR_GB = (1/8) * np.asarray([
    [0., 0., -1., 0., 0.],
    [0., 0., 2., 0., 0.],
    [-1., 2., 4., 2., -1.],
    [0., 0., 2., 0., 0.],
    [0., 0., -1., 0., 0.],
])

#R at green in R row, B column or B at green in B row, R colunm
Rg_RB_Bg_BR = (1/8) * np.asarray([
    [0., 0., .5, 0., 0.],
    [0., -1., 0., -1., 0.],
    [-1., 4., 5., 4., -1.],
    [0., -1., 0., -1., 0.],
    [0., 0., .5, 0., 0.],
])

# R at green in B row, R column or B at green in R row, B column 
Rg_BR_Bg_RB = (1/8) * np.asarray([
    [0., 0., -1, 0., 0.],
    [0., -1., 4., -1., 0.],
    [.5, 0., 5., 0., .5],
    [0., -1., 4., -1., 0.],
    [0., 0., -1, 0., 0.],
])

#R at blue in B row, B column or B at red in R row, R column
Rb_BB_Br_RR = (1/8) * np.asarray([
    [0., 0., -1.5, 0., 0.],
    [0., 2., 0., 2., 0.],
    [-1.5, 0., 6., 0., -1.5],
    [0., 2., 0., 2., 0.],
    [0., 0., -1.5, 0., 0.],
])

